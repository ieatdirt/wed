#!/usr/bin/python
import sys
import os 
import stat

helpstatement = """

Run the program like "wed [file]"
Commands are as follows:
println, prints a line
writeln, replaces a line, written to the real file when exiting
exit, exits the program safely
extendbuf, adds more lines to the file
shrinkbuf, removes lines from the file
help, prints help

"""

def whattype(path):
    if os.path.exists(path):
        filetype = os.stat(path).st_mode
        if stat.S_ISBLK(filetype):
            return "Block device"
        elif stat.S_ISREG(filetype):
            return "Normal file"
        elif stat.S_ISCHR(filetype):
            return "Character device"
        elif stat.S_ISDIR(filetype):
            return "Directory"
        else: 
            return "Unknown"   
    else:
        print("Path does not exist.")
        sys.exit(1)

#enter main function

if len(sys.argv) == 1: 
    print("No arguments provided.")
    print(helpstatement)
    sys.exit(1)
elif len(sys.argv) > 2:
    print("Too many arguments")
    print(helpstatement)
    sys.exit(1)

filetype = whattype(sys.argv[1])

if filetype != "Normal file":
    print("File specified is not a file, is a " + filetype)
    sys.exit(1)

file = open(sys.argv[1], "r")
filelinecontents = file.readlines()

file.close()
file = open(sys.argv[1], "w")

while 1:
    try:
        inp = input("Enter command.    ")
        match inp:
            case "println":
                arg = int(input("What line? Starts from 0,    "))
                if 0 > arg or arg >= len(filelinecontents):
                    print("Invalid file index")
                    continue

                print("\n", arg, " ; ", filelinecontents[arg], sep="")
            case "exit":
                sys.exit(0)
            case "help":
                print(helpstatement)
            case "writeln":
                
                linetoreplace = int(input("What line? Starts from 0,    "))
                if 0 > linetoreplace or linetoreplace >= len(filelinecontents):
                    print("Invalid file index")
                    continue

                replacementline = input("Replacing with,    ") + "\n"
                filelinecontents[linetoreplace] = replacementline
            case "printbuf":
                print()
                for lnum, line in enumerate(filelinecontents):
                    print(lnum, "; ", end="")
                    print(line, end="")
                print()
            case "extendbuf":
                arg = int(input("How many new lines?    "))
                if 0 > arg:
                    print("You can't add a negative number of lines")
                    continue

                for _ in range(arg):
                    filelinecontents.append("\n")
            case "shrinkbuf":
                arg = int(input("How many lines?    "))
                if 0 > arg:
                    print("You can't remove a negative number of lines")
                    continue

                for _ in range(arg):
                    filelinecontents.pop()
            case _: 
                print("No known command entered")
    except SystemExit as se:
        file.writelines(filelinecontents)
        file.close()
        sys.exit(se.code)
    except:
        print("\nSomething went wrong!!! Attempting safe exit")
        print(sys.exc_info())
        file.writelines(filelinecontents)
        file.close()
        sys.exit(1)